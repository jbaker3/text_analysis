import json


def get_cred():
    
    twitter_cred=dict()

    twitter_cred['API_KEY']=input('API Key: ')
    twitter_cred['API_SECRET']=input('API secret: ')
    twitter_cred['ACCESS_TOKEN']=input('Access Token: ')
    twitter_cred['ACCESS_SECRET']=input('Access Secret: ')

    with open('twitter_credentials.json', 'w') as secret_info:
        json.dump(twitter_cred, secret_info, indent=4, sort_keys=True)
