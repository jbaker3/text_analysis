import wordcloud
import matplotlib.pyplot as plt
import nltk


def mk_cloud():
    f=open('tweets.txt', 'r')
    text=f.read()
    f.close()

    nltk.download('stopwords')
    stopWords = set(nltk.corpus.stopwords.words('english'))

    cloud = wordcloud.WordCloud(stopwords=stopWords, background_color="white").generate(text)

    plt.figure(figsize=[30,15])
    plt.imshow(cloud, interpolation='bilinear')
    plt.axis("off")
    plt.show()
